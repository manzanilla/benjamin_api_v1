function isDevelopmentMode() {
  return process.env.NODE_ENV === 'development'
}

module.exports = {
  isDevelopmentMode
}
