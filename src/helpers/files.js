module.exports = { getExtIdByExtName }

function getExtIdByExtName(extName) {
  if (extName === 'jpg') return 1
  if (extName === 'png') return 2
  if (extName === 'webp') return 3
}
