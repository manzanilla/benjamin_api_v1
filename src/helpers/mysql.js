const util = require('util')

async function getConnection(pool) {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        reject(err)
      } else resolve(connection)
    })
  }).catch((reason) => {
    console.error(reason)
  })
}

function getQuery(connection) {
  return util.promisify(connection.query).bind(connection)
}

module.exports = {
  getConnection,
  getQuery
}
