const LANGUAGES = [
  { ID: 1, CODE: 'uk' },
  { ID: 2, CODE: 'ru' }
]

const LANGUAGES_BY_CODE = {}

for (const LANGUAGE of LANGUAGES) {
  const { CODE } = LANGUAGE
  LANGUAGES_BY_CODE[CODE] = LANGUAGE
}

module.exports = {
  LANGUAGES,
  LANGUAGES_BY_CODE
}
