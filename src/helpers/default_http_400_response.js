const { ReasonPhrases, StatusCodes } = require('http-status-codes')
const envHelper = require('./env')

module.exports = function (e, ctx) {
  console.error(e)
  ctx.status = StatusCodes.BAD_REQUEST

  if (envHelper.isDevelopmentMode()) {
    ctx.body = { message: e.toString() }
  } else {
    ctx.body = { message: ReasonPhrases.BAD_REQUEST }
  }
}
