const DATA_TYPE = [
  { id: 1, name: 'number' },
  { id: 4, name: 'string' }
]

const DATA_TYPE_BY_ID = {}
const DATA_TYPE_BY_NAME = {}

for (const el of DATA_TYPE) {
  const { id, name } = el

  DATA_TYPE_BY_ID[id] = { name }
  DATA_TYPE_BY_NAME[name] = { id }
}

module.exports = {
  DATA_TYPE,
  DATA_TYPE_BY_ID,
  DATA_TYPE_BY_NAME
}
