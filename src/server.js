const { resolve } = require('path')
const Koa = require('koa')
const cors = require('@koa/cors')
const app = new Koa()
const yamljs = require('yamljs')
const { koaSwagger } = require('koa2-swagger-ui')

require('dotenv').config()

app.use(cors())

const spec = yamljs.load(resolve(__dirname, '../openapi.yaml'))
app.use(
  koaSwagger({
    routePrefix: '/swagger/docs',
    swaggerOptions: { spec }
  })
);

require('./store/mongodb/connection')

app.use(require('./routes/api_info').routes())
app.use(require('./routes/test').routes())
app.use(require('./routes/languages').routes())
app.use(require('./routes/categories').routes())
app.use(require('./routes/products').routes())
app.use(require('./routes/files').routes())

app.listen(process.env.API_PORT, () => {
  const message = `Server listening on ${process.env.API_IP}:${process.env.API_PORT}`
  console.log(message)
})
