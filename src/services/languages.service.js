// const mysqlPool = require('../store/mysql')
// const mysqlHelper = require('../helpers/mysql')

const LANGUAGES = [{
    id: 1,
    name: 'Українська',
    code: 'uk'
  }, {
    id: 2,
    name: 'Русский',
    code: 'ru'
  }
]

const LANGUAGE_BY_ID = []
const LANGUAGE_BY_CODE = []
const CODE_BY_ID = {}
const ID_BY_CODE = {}

for (const language of LANGUAGES) {
  const { id, code } = language
  CODE_BY_ID[id] = code
  ID_BY_CODE[code] = id
  LANGUAGE_BY_ID[id] = language
}

/*async function getLanguagesPromise() {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const qs = 'SELECT * FROM languages'
  const result = await query(qs)
  connection.release()
  return result
}*/

function getLanguages() {
  return LANGUAGES
}

/*async function getLanguageByIdPromise(id) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const qs = 'SELECT l.* FROM languages l WHERE l.id=?'
  const [ result ] = await query(qs, id)
  connection.release()
  return result
}*/

function getLanguageById(id) {
  return LANGUAGE_BY_ID[id]
}

/*async function getLanguageByCodePromise(code) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const qs = 'SELECT l.* FROM languages l WHERE l.code=?'
  const [ result ] = await query(qs, code)
  connection.release()
  return result
}*/

function getLanguageByCode(code) {
  return LANGUAGE_BY_CODE[code]
}

function getLanguageIdByLanguageCode(code) {
  return ID_BY_CODE[code]
}

function getLanguageCodeByLanguageId(id) {
  return CODE_BY_ID[id]
}

function languageIdExists(id) {
  return !!(CODE_BY_ID[id])
}

function languageCodeExists(code) {
  return !!(ID_BY_CODE[code])
}

module.exports = {
  getLanguages,
  // getLanguagesPromise,
  getLanguageById,
  // getLanguageByIdPromise,
  getLanguageByCode,
  // getLanguageByCodePromise,
  getLanguageIdByLanguageCode,
  getLanguageCodeByLanguageId,
  languageIdExists,
  languageCodeExists
}
