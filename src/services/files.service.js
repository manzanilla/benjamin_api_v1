const mysqlPool = require('../store/mysql')
const mysqlHelper = require('../helpers/mysql')
const { getExtIdByExtName } = require('../helpers/files')

module.exports = {
  saveFiles,
  removeFilesByDirs
}

async function getFileIDsByDirs(dirs) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const IN = Array(dirs.length).fill('?').join(',')
  const qs = `SELECT id FROM files WHERE dir IN (${IN})`
  const results = await query(qs, dirs)
  connection.release()
  return results.map(({ id }) => id)
}

async function removeFilesByDirs(dirs) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const fileIDs = await getFileIDsByDirs(dirs)
  const IN = fileIDs.join(',')
  await query(`DELETE FROM products_images_main WHERE files_id IN(${IN})`)
  await query(`DELETE FROM products_images WHERE files_id IN(${IN})`)
  await query(`DELETE FROM files WHERE id IN(${IN})`)
  return fileIDs
}

function getFileInfoByUrl(fileUrl) {
  const chunksByDot = fileUrl.split('.')
  const chunksBySlash = fileUrl.split('/')
  const ext = chunksByDot[chunksByDot.length - 1]
  const extId = getExtIdByExtName(ext)
  const dir = chunksBySlash[chunksBySlash.length - 2]

  return { dir, extId }
}

async function saveFiles(urls, productIds) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)

  let values = []
  const params = []
  for (const fileURL of urls) {
    const { dir, extId } = getFileInfoByUrl(fileURL)

    params.push(extId)
    params.push(dir)
    values.push('(?,?)')
  }

  values = values.join(',')

  let qs = `INSERT INTO files (file_extensions_id, dir) VALUES ${values}`

  const { affectedRows } = await query(qs, params)

  const getLastNFiles = await getLastNFiles(affectedRows)

  if (Array.isArray(productIds)) {
    const fileIds = getLastNFiles.map(({ id }) => id)
    await productsImagesConnector(productIds, fileIds)
  }

  return getLastNFiles
}

async function productsImagesConnector(productIds, fileIds) {
  const values = []
  const params = []

  for (const productId of productIds) {
    for (const fileId of fileIds) {
      values.push('(?,?)')
      params.push(productId)
      params.push(fileId)
    }
  }

  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const qs = `INSERT INTO products_images VALUES ${values.join(',')}`
  const result = await query(qs, params)
  connection.release()

  return result
}

async function getLastNFiles(limit) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const qs = `SELECT * FROM files ORDER BY id DESC LIMIT ?`
  const result = await query(qs, limit)
  connection.release()
  return result
}
