const mysqlPool = require('../store/mysql')
const mysqlHelper = require('../helpers/mysql')
const { DATA_TYPE_BY_NAME } = require('../helpers/dataType')

module.exports = {
  getProduct,
  updateProductJson,
  getMetaKeys,
  getMetaValues,
  getMeta,
  getImages,
  getMainImageByProductId,
  getIds
}

async function getImagesCount() {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const qs = `SELECT count(*) as count FROM files f
  INNER JOIN products_images pi
  ON pi.products_id=f.id`
  const { 0: { count = 0 } = {} } = await query(qs)
  connection.release()
  return count
}

async function getImages(limit = 20, offset = 0) {
  // TODO check limit/offset
  /*const checkLimit = (limit) => {
    limit = +limit
    if (isNaN(limit) || limit < 1 || limit > 20) throw 'Invalid limit query param'
  }
  const checkOffset = () => {

  }
  checkLimit(limit)
  checkOffset(offset)*/

  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const count = await getImagesCount()
  if (count > 0) {
    const qs = `SELECT * FROM files f
    INNER JOIN products_images pi
    ON pi.products_id=f.id LIMIT ${offset}, ${limit}`
    const results = await query(qs)
    connection.release()
    return { count, results }
  }
  return { count }
}

async function getMainImageByProductId(productId) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const qs = `SELECT f.dir, f.file_extensions_id AS ext
  FROM files f
  INNER JOIN  products_images_main pim ON f.id=pim.files_id
  WHERE pim.products_id=?`
  const results = await query(qs, productId)
  connection.release()

  const {
    0: {
      dir,
      ext
    } = {}
  } = results

  if (!dir || !ext) return null

  return { dir, ext }
}

async function getProduct(productId, languageId) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const qs = `SELECT p.id, p.name, pj.json, pj.last_json_update
  FROM products p
  LEFT JOIN products_json pj
  ON (pj.products_id=p.id AND pj.languages_id=?)
  WHERE p.id=?`

  const [ result = {} ] = await query(qs, [ languageId, productId ])
  connection.release()

  return result
}

async function updateProductJson(productId, languageId, json) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)

  json = JSON.stringify(json)

  const qs = `INSERT INTO products_json VALUES (?, ?, ?, now())
  ON DUPLICATE KEY UPDATE json=?, last_json_update=now()`

  await query(qs, [ productId, languageId, json, json ])
  connection.release()
}

async function getMetaKeys(languageId) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)

  const qs = `
    SELECT pmk.*, pmkt.name AS name_trans
    FROM products_meta_keys pmk
    LEFT JOIN products_meta_keys_trans pmkt
    ON (pmk.id = pmkt.products_meta_keys_id AND pmkt.languages_id=?)`

  let metaKeys = await query(qs, languageId)
  connection.release()

  for (const el of metaKeys) {
    el.name = el.name_trans ? el.name_trans : el.name
    delete el.name_trans

    if (el.data_type_id === null) {
      delete el.data_type_id
    }
  }

  return metaKeys
}

async function getMetaValues(metaKeyId, languageId) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const qs = `
  SELECT pmv.*, pmk.data_type_id, pmvt.value AS value_trans
  FROM products_meta_values pmv
  INNER JOIN products_meta_keys pmk
  ON pmv.products_meta_keys_id=pmk.id
  LEFT JOIN products_meta_values_trans pmvt
  ON (pmv.id = pmvt.products_meta_values_id AND pmvt.languages_id=?)
  WHERE pmv.products_meta_keys_id=?`

  const metaValues = await query(qs, [ languageId, metaKeyId ])
  connection.release()

  for (const el of metaValues) {
    if (DATA_TYPE_BY_NAME.number.id === el.data_type_id) {
      el.value = +el.value
    } else {
      el.value = el.value_trans ? el.value_trans : el.value
    }

    delete el.value_trans
    delete el.products_meta_keys_id
    delete el.data_type_id
  }

  return metaValues
}

async function getMeta(productId, languageId, format) {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)

  const qs = `
  SELECT
    pmv.id,
    pmv.products_meta_keys_id AS key_id,
    pmv.value,
    pmk.name AS key_name,
    pmkt.name AS key_name_trans,
    pmk.data_type_id,
    pmvt.value AS value_trans,
    pm.*
  FROM products_meta pm
  
  LEFT JOIN products_meta_values pmv
  ON (pmv.id = pm.products_meta_values_id)
  
  INNER JOIN products_meta_keys pmk
  ON pmv.products_meta_keys_id = pmk.id

  LEFT JOIN products_meta_keys_trans pmkt
  ON (pmk.id=pmkt.products_meta_keys_id AND pmkt.languages_id=?)

  LEFT JOIN products_meta_values_trans pmvt
  ON (pmv.id = pmvt.products_meta_values_id AND pmvt.languages_id=?)
  
  WHERE pm.products_id=?`

  const metaArray = await query(qs, [ languageId, languageId, productId ])
  connection.release()

  const metaObject = {}

  for (const el of metaArray) {
    const { key_id, data_type_id, value_trans, key_name_trans } = el

    if (DATA_TYPE_BY_NAME.number.id === data_type_id) {
      el.value = +el.value
    } else {
      el.value = value_trans ? value_trans : el.value
    }

    el.key_name = key_name_trans ? key_name_trans : el.key_name

    delete el.key_name_trans
    delete el.value_trans
    delete el.products_id
    delete el.products_meta_values_id
    delete el.data_type_id

    if (format === 1) {
      if ([1, 2, 3, 6, 7, 8, 9, 10].indexOf(key_id) > -1) {
        metaObject[key_id] = el
      } else {
        const { key_name } = el

        if (!metaObject[key_id]) {
          metaObject[key_id] = { key_name }
          metaObject[key_id].values = []
        }

        metaObject[key_id].values.push(el)

        delete el.key_name
      }

      delete el.key_id
    }
  }

  if (format === 1) {
    return metaObject
  }

  return metaArray
}

async function getIds() {
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  const qs = 'SELECT id FROM products'

  const ids = await query(qs)
  connection.release()

  return ids.map(({ id }) => id)
}
