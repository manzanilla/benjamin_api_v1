const mysqlPool = require('../store/mysql')
const mysqlHelper = require('../helpers/mysql')

function makeCategoriesTree(nodes, parentId) {
  nodes = nodes
    .filter((node) => node.parentId === parentId)
    .reduce(
      (tree, node) => [
        ...tree,
        {
          ...node,
          children: makeCategoriesTree(nodes, node.id),
        },
      ],
      [],
    )

  return nodes
}

function clearCategoriesTree(categories) {
  categories.map((el) => {
    delete el.status
    delete el.parentId
    if (!el.children.length) delete el.children
    else clearCategoriesTree(el.children)
  })
}

async function getCategories(searchParams = {}) {
  const {
    languageId,
    status
  } = searchParams
  const qsParams = [ languageId ]
  const connection = await mysqlHelper.getConnection(mysqlPool)
  const query = mysqlHelper.getQuery(connection)
  let qs = `SELECT
  c.id,
  c.name,
  c.h1,
  c.parent_id as parentId,
  ct.name AS nameTrans,
  ct.h1 AS h1Trans
  FROM categories c
  LEFT JOIN categories_trans ct
  ON (c.id = ct.categories_id AND ct.languages_id=?)`

  if (status) {
    qs += ` WHERE c.status=?`
    qsParams.push(status)
  }

  let result = await query(qs, qsParams)

  result.forEach((el) => {
    if (el.nameTrans) {
      el.name = el.nameTrans
    }

    if (el.h1Trans) {
      el.h1 = el.h1Trans
    } else if (!el.h1) {
      delete el.h1
    }

    if (!el.parentId) {
      delete el.parentId
    }

    delete el.nameTrans
    delete el.h1Trans
    delete el.status
  })

  connection.release()

  return result
}

module.exports = {
  getCategories,
  makeCategoriesTree,
  clearCategoriesTree
}
