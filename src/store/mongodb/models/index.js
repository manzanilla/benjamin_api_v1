const mongoose = require('mongoose')

const { ProductsSchema } = require('./Products')
const { CategoriesSchema } = require('./Categories')

module.exports = {
  Products: mongoose.model('Products', ProductsSchema),
  Categories: mongoose.model('Categories', CategoriesSchema)
}
