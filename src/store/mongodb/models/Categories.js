const mongoose = require('mongoose')
const Schema = mongoose.Schema

const definition = {
  name_uk: { type: String, required: true },
  name_ru: { type: String, required: true },
  parent: { type: String, default: null },
  status: { type: Number, required: true, enum: [ 0, 1 ], default: 0 }
}

const options = {
  collection: 'Categories',
  timestamps: true
}


const CategoriesSchema = new Schema(definition, options)

module.exports = { CategoriesSchema }
