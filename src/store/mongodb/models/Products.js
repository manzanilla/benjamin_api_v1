const mongoose = require('mongoose')
const Schema = mongoose.Schema

const h1 = {
  type: String,
  required: true
}

let ProductsSchema = new Schema(
  {
    id: {
      type: Number,
      min: 1,
      unique: true,
      required: true
    },
    mainImage: {
      dir: String,
      ext: { type: Number, enum: [ 1, 2, 3 ] }
    },
    h1_ru: h1,
    h1_uk: h1,
    price: Number, // Price
    mk_2: Number, // Brand
    mk_3: Number, // Size
    mk_4: [ Number ], // Colors
    mk_5_values: [ Number ], // Category IDs
    mk_6: Number // Model
  },
  {
    collection: 'Products',
    timestamps: true
  }
)

module.exports = { ProductsSchema }
