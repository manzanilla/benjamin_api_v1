const mongoose = require('mongoose')
const {
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_DB_NAME
} = process.env

let uri = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}`
uri += `@localhost:27017/${MONGO_DB_NAME}?authSource=admin`

mongoose.connect(uri, {
  useUnifiedTopology: true,
  useNewUrlParser: true
}, (error) => {
  if (error) console.error(error)
})

mongoose.connection.once('open', () => {
  console.log('MongoDB database connection established successfully')
})
