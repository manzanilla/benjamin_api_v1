const mysql = require('mysql')
const pool = mysql.createPool({
  connectionLimit: 20,
  host: process.env.MYSQL_DB_HOST,
  user: process.env.MYSQL_DB_USER,
  password: process.env.MYSQL_DB_PASSWORD,
  database: process.env.MYSQL_DB_NAME
})

/*pool.on('connection', function (connection) {
  console.log('MySQL connection is successful')
})

pool.on('acquire', function (connection) {
  console.log('MySQL connection %d acquired', connection.threadId)
})

pool.on('release', function (connection) {
  console.log('MySQL connection %d released', connection.threadId)
})*/

module.exports = pool
