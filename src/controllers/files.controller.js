module.exports = {
  saveFiles,
  removeFilesByDirs
}

const filesService = require('../services/files.service')

async function saveFiles(ctx) {
  try {
    const files = ctx.request.body
    let productIds = ctx?.request?.query?.productIds || undefined

    if (productIds && typeof productIds === 'string') {
      productIds = JSON.parse(productIds)
    }

    if (!Array.isArray(files) || !files.length) {
      onCatch('Bad request (files)', ctx)

      return
    }

    ctx.body = await filesService.saveFiles(files, productIds)
  } catch (e) {
    onCatch(e, ctx)
  }
}

async function removeFilesByDirs(ctx) {
  try {
    const dirs = ctx.request.body

    if (!Array.isArray(dirs) || !dirs.length) {
      onCatch('Bad request (dirs)', ctx)

      return
    }

    ctx.body = await filesService.removeFilesByDirs(dirs)
  } catch (e) {
    onCatch(e, ctx)
  }
}

const onCatch = (e, ctx) => {
  console.error(e)

  ctx.status = 400
  ctx.body = { message: e.toString() }
}
