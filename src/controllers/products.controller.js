const { LANGUAGES_BY_CODE } = require('../helpers/languages')
const productService = require('../services/products.service')
const { Products } = require('../store/mongodb/models')

const getMetaKeys = async (ctx) => {
  try {
    const languageId = LANGUAGES_BY_CODE[ctx.params.languageCode].ID
    ctx.body = await productService.getMetaKeys(languageId)
  } catch (e) {
    onCatch(e, ctx)
  }
}

const getMetaValues = async (ctx) => {
  try {
    const languageId = LANGUAGES_BY_CODE[ctx.params.languageCode].ID
    let { id: metaKeyId } = ctx.params
    metaKeyId = +metaKeyId

    // ctx.set('Cache-Control', 'max-age=300')

    ctx.body = await productService.getMetaValues(metaKeyId, languageId)
  } catch (e) {
    onCatch(e, ctx)
  }
}

const getMeta = async (ctx) => {
  const languageId = LANGUAGES_BY_CODE[ctx.params.languageCode].ID
  let { id: productId } = ctx.params
  productId = +productId
  let { format } = ctx.request.query

  if (format) format = +format

  // ctx.set('Cache-Control', 'max-age=300')

  ctx.body = await productService.getMeta(productId, languageId, format)
}

const getProductById = async (ctx) => {
  try {
    const { languageCode } = ctx.params
    const languageId = LANGUAGES_BY_CODE[languageCode].ID
    let { id: productId } = ctx.params
    productId = +productId

    let {
      id,
      name: productName,
      json
    } = await productService.getProduct(productId, languageId)

    const withoutCache = ctx.request.query.cache === '2'

    if (!id) {
      ctx.status = 404
      ctx.body = { message: 'Product not found' }

      return
    }

    // TODO: lastJsonUpdate + 5m
    if (json && !withoutCache) {
      ctx.type = 'json'
      ctx.body = json

      return
    }

    const meta = await productService.getMeta(productId, languageId, 1)
    const product = {}

    product.h1 = meta[7] ? meta[7].value : `${productName} ${languageCode}#${productId}`
    product.title = meta[8] ? meta[8].value : productName
    product.metaDescription = meta[9] ? meta[9].value : productName
    product.htmlDescription = meta[10] ? meta[10].value : undefined
    product.price = meta[1] ? meta[1].value : undefined

    productService
      .updateProductJson(productId, languageId, product)
      .catch((reason) => {
        console.error(reason)
      })

    ctx.body = product
  } catch (e) {
    onCatch(e, ctx)
  }
}

const getIds = async (ctx) => {
  try {
    ctx.body = await productService.getIds()
  } catch (e) {
    onCatch(e, ctx)
  }
}

async function getMainImageByProductId(ctx) {
  try {
    const { id: productId } = ctx.params

    const mainImage = await productService.getMainImageByProductId(productId)

    if (!mainImage) {
      onCatch('Main image not found', ctx, 404)
    } else {
      ctx.body = mainImage
    }
  } catch (e) {
    onCatch(e, ctx)
  }
}

const search = async (ctx) => {
  try {
    const { searchParams = '' } = ctx.params
    const { languageCode } = ctx.params
    let { offset } = ctx.request.query
    const limit = 16
    offset = offset ? 0 : offset

    const filter = getSearchFilterBySearchParams(searchParams)
    const total = await Products.countDocuments(filter)
    let list = []

    if (total) {
      list = await Products
        .find(filter)
        .select({
          id: 1,
          [`h1_${languageCode}`]: 1,
          _id: 0,
          price: 1,
          mainImage: 1
        })
        .limit(limit)
        .skip(offset)
        .lean()
    }

    ctx.body = { total, list }
  } catch (e) {
    onCatch(e, ctx)
  }
}

async function getImages(ctx) {
  try {
    const limit = ctx.request.query.limit,
      offset = ctx.request.query.offset
    ctx.body = await productService.getImages(limit, offset)
  } catch (e) {
    onCatch(e, ctx)
  }
}

const getSearchFilterBySearchParams = (searchParams) => {
  let mk_5_values
  searchParams.split('/').map((param) => {
    if (param.startsWith('mk5values')) { //
      param = param.split('_')
      param.shift()
      mk_5_values = param
        .filter((el) => /^[0-9]+$/.test(el))
        .map((el) => +el)
    }
  })

  const filter = {}

  if (mk_5_values) {
    filter.mk_5_values = { $in: mk_5_values }
  }

  return filter
}

const onCatch = (e, ctx, status = 400) => {
  console.error(e)

  ctx.status = status
  ctx.body = { message: e.toString() }
}

module.exports = {
  getProductById,
  getMeta,
  getMetaKeys,
  getMetaValues,
  getImages,
  search,
  getMainImageByProductId,
  getIds
}
