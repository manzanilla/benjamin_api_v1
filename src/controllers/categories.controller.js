const categoriesService = require('../services/categories.service')
const languagesService = require('../services/languages.service')
const envHelper = require('../helpers/env')
const { ReasonPhrases, StatusCodes } = require('http-status-codes')

const CATEGORIES_VIEW_FORMAT = {
  DEFAULT: 'default',
  TREE: 'tree',
}

async function getCategories(ctx) {
  try {
    const {
      languageCode,
      status,
      format = CATEGORIES_VIEW_FORMAT.TREE
    } = ctx.request.query
    const languageId = languagesService.getLanguageIdByLanguageCode(languageCode)

    if (!languageId) {
      ctx.status = StatusCodes.BAD_REQUEST
      ctx.body = { message: 'Language not found' }
      return
    }

    const searchParams = { languageId, status }
    let result = await categoriesService.getCategories(searchParams)

    if (format === CATEGORIES_VIEW_FORMAT.TREE) {
      result = categoriesService.makeCategoriesTree(result)
      categoriesService.clearCategoriesTree(result)
    }

    if (!envHelper.isDevelopmentMode()) {
      ctx.set('Cache-Control', 'max-age=300')
    }

    ctx.status = StatusCodes.OK
    ctx.body = result
  } catch (e) {
    console.error(e)
    ctx.status = StatusCodes.BAD_REQUEST

    if (envHelper.isDevelopmentMode()) {
      ctx.body = { message: e.toString() }
      return
    }

    ctx.body = { message: ReasonPhrases.BAD_REQUEST }
  }
}

module.exports = {
  getCategories
}
