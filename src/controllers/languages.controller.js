const languagesService = require('../services/languages.service')
const envHelper = require('../helpers/env')
const { ReasonPhrases, StatusCodes } = require('http-status-codes')

const getLanguages = async (ctx) => {
  try {
    const result = await languagesService.getLanguages()
    ctx.set('Cache-Control', 'max-age=300')
    ctx.status = StatusCodes.OK
    ctx.body = result
  } catch (e) {
    console.error(e)
    ctx.status = StatusCodes.BAD_REQUEST

    if (envHelper.isDevelopmentMode()) {
      ctx.body = { message: e.toString() }
      return
    }

    ctx.body = { message: ReasonPhrases.BAD_REQUEST }
  }
}

async function getLanguageById(ctx) {
  try {
    const id = +ctx.params.id
    const result = await languagesService.getLanguageById(id)

    if (!result) {
      ctx.status = StatusCodes.NOT_FOUND
      ctx.body = { message: 'Language not found' }
      return
    }

    ctx.set('Cache-Control', 'max-age=300')
    ctx.status = StatusCodes.OK
    ctx.body = result
  } catch (e) {
    console.error(e)
    ctx.status = StatusCodes.BAD_REQUEST

    if (envHelper.isDevelopmentMode()) {
      ctx.body = { message: e.toString() }
      return
    }

    ctx.body = { message: ReasonPhrases.BAD_REQUEST }
  }
}

async function getLanguageByCode(ctx) {
  try {
    const code = ctx.params.code
    const result = await languagesService.getLanguageByCode(code)

    if (!result) {
      ctx.status = StatusCodes.NOT_FOUND
      ctx.body = { message: 'Language not found' }
      return
    }

    ctx.set('Cache-Control', 'max-age=300')
    ctx.status = StatusCodes.OK
    ctx.body = result
  } catch (e) {
    console.error(e)
    ctx.status = StatusCodes.BAD_REQUEST

    if (envHelper.isDevelopmentMode()) {
      ctx.body = { message: e.toString() }
      return
    }

    ctx.body = { message: ReasonPhrases.BAD_REQUEST }
  }
}

module.exports = {
  getLanguages,
  getLanguageById,
  getLanguageByCode
}
