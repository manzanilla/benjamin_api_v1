const router = require('koa-router')({
  sensitive: true
})
const productsController = require('../controllers/products.controller')

router.get('/:languageCode(uk|ru)/products/meta-keys', productsController.getMetaKeys)
router.get('/:languageCode(uk|ru)/products/meta-keys/:id(\\d+)/values', productsController.getMetaValues)
router.get('/:languageCode(uk|ru)/products/:id(\\d+)/meta', productsController.getMeta)
router.get('/:languageCode(uk|ru)/products/:id(\\d+)', productsController.getProductById)
router.get('/products/ids', productsController.getIds)
router.get('/products/images', productsController.getImages)
router.get('/products/:id/main-image', productsController.getMainImageByProductId)
router.get('/:languageCode(uk|ru)/products/search/:searchParams*', productsController.search)

module.exports = router
