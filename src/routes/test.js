const router = require('koa-router')({
  prefix: '/test',
  sensitive: true
})

router.get('/ok', (ctx) => {
  ctx.body = 'ok'
})

module.exports = router
