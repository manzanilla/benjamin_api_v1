const router = require('koa-router')({
  prefix: '/languages',
  sensitive: true
})
const languagesController = require('../controllers/languages.controller')

router.get('/', languagesController.getLanguages)
router.get('/:id(\\d+)', languagesController.getLanguageById)
router.get('/:code(uk|ru)', languagesController.getLanguageByCode)

module.exports = router
