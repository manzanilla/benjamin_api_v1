const koaBody = require('koa-body')
const router = require('koa-router')({
  sensitive: true
})
const filesController = require('../controllers/files.controller')

router.post('/files/urls', koaBody(), filesController.saveFiles)
router.post('/files/delete-by-dirs', koaBody(), filesController.removeFilesByDirs)

module.exports = router
