const fs = require('fs')
const { resolve } = require('path')
const router = require('koa-router')({
  sensitive: true
})

router.get('/', (ctx) => {
  const path = resolve(__dirname, '../../package.json')
  const rawData = fs.readFileSync(path)
  const { version } = JSON.parse(rawData)
  ctx.body = { version }
})

module.exports = router
