const categoriesController = require('../controllers/categories.controller')
const router = require('koa-router')({
  prefix: '/categories',
  sensitive: true
})

router.get('/', categoriesController.getCategories)

module.exports = router
