openapi: 3.0.0
info:
  title: Benjamin API V1
  description: Benjamin API V1
  version: 1.0.3

servers:
  - url: http://api.v1.benjamin.com.localhost
    description: Internal staging server for testing
  - url: https://api.v1.benjamin.com.ua
    description: Main (production) server

components:
  schemas:
    Category:
      type: object
      properties:
        id:
          type: number
          example: 12
        name:
          type: string
          example: 'Термобілизна'
        children:
          type: array
          items:
            type: object
            properties:
              id:
                type: number
                example: 13
              name:
                type: string
                example: 'Жіноча'
        parentId:
          type: number
          example: 3
        h1:
          type: string
          example: 'Дитяча термобілизна'
      required:
        - id
        - name
    Languages:
      type: object
      properties:
        id:
          type: number
          example: 1
        name:
          type: string
          example: 'Українська'
        code:
          type: string
          example: 'uk'
      required:
        - id
        - name
        - code

paths:
  /languages:
    get:
      tags:
        - Languages
      summary: Returns a list of languages
      description: Returns a list of languages
      responses:
        '200': # status code
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Languages'
  /languages/{languageId}:
    get:
      tags:
        - Languages
      summary: Returns a language by ID
      description: Returns a language by ID
      parameters:
        - name: languageId
          in: path
          required: true
          description: language ID
          schema:
            type: number
            enum: [1, 2]
      responses:
        '200': # status code
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Languages'
  /languages/{code}:
    get:
      tags:
        - Languages
      summary: Reteruns a language by ISO 639-1 Code
      description: Reteruns a language by ISO 639-1 Code
      parameters:
        - name: code
          in: path
          required: true
          description: ISO 639-1 Code
          schema:
            type: string
            enum: [uk, ru]
      responses:
        '200': # status code
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Languages'
  /categories:
    get:
      tags:
        - Categories
      summary: Returns a list of categories
      description: Returns a list of categories
      parameters:
        - in: query
          name: languageCode
          required: true
          schema:
            type: string
            enum: [uk, ru]
            default: uk
          description: |
            ISO 639-1 Code

             * `uk` - Ukrainian

             * `ru` - Russian
        - in: query
          name: status
          required: false
          description: |
            Category status

             * `0` - Inactive

             * `1` - Active

          schema:
            type: integer
            default: 1
            enum: [0, 1]
        - in: query
          name: cache
          required: false
          description: |
            With cache or not

             * `0` - Without cache

             * `1` - Cached result

          schema:
            type: integer
            default: 0
            enum: [0, 1]
        - in: query
          name: format
          required: false
          description: |
            View format

             * `default` - Simple list

             * `tree` - Tree (with child elements)
          schema:
            type: string
            default: tree
            enum:
              - default
              - tree
      responses:
        '200': # status code
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Category'
  /products:
    get:
      tags:
        - Products
      summary: Returns list of products
      description: Returns list of products
      parameters:
        - in: query
          name: languageCode
          required: true
          schema:
            type: string
            enum: [uk, ru]
            default: uk
          description: |
            ISO 639-1 Code

             * `uk` - Ukrainian

             * `ru` - Russian
        - in: query
          name: limit
          required: false
          schema:
            type: integer
            enum: [2, 4, 8, 16, 32, 64]
            default: 16
          description: Maximum number of products in search results per page
      responses:
        '200': # status code
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  total:
                    type: integer
                    example: 1
                    description: Общее количество товаров
                  list:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: number
                          example: 1
                        h1:
                          type: string
                          example: Термобелье Stimma мужское 0024 S
                        price:
                          type: number
                          example: 499
                        mainImage:
                          type: object
                          properties:
                            dir:
                              type: string
                              example: 2IToihgmQ
                            ext:
                              type: number
                              enum: [1, 2, 3]
                              example: 1
                              description: Extension ID
