module.exports = {
  'env': {
    'commonjs': true,
    'es2021': true,
    'node': true,
  },
  'extends': [
    'google',
    'plugin:prettier/recommended',
  ],
  'parserOptions': {
    'ecmaVersion': 13,
  },
  'plugins': ['prettier'],
  'rules': {
  },
};
